package test1;

public class Rabbit 
{
	
		private int xPosition;
		private int yPosition;
		
		public Rabbit(int x,int y) 
		{
			this.xPosition=x;
			this.yPosition=y;
		}
		
		// -----------
		// METHODS 
		// ------------
		public void printCurrentPosition() {
			System.out.println("The current position of the rabbit is x : "+this.xPosition+" y :"+this.yPosition);
		}

		public void sayHello() {
			System.out.println("Hello! I am a rabbit!");
		}

		
		
		// ----------------
		// ACCESSOR METHODS
		// ----------------
		public int getxPosition() {
			return xPosition;
		}

		public void setxPosition(int xPosition) {
			this.xPosition = xPosition;
		}

		public int getyPosition() {
			return yPosition;
		}

		public void setyPosition(int yPosition) {
			this.yPosition = yPosition;
		}
		
		

}
