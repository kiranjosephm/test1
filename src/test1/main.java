package test1;
public class main {

	public static void main(String[] args) throws InterruptedException {
		Rabbit rabbit = new Rabbit(100,100);
		rabbit.sayHello();
		
		int p=15;
		boolean runForever = true;

		while (runForever == true) {
			 if(rabbit.getxPosition()>=400)
					p=-15;
			else if(rabbit.getxPosition()<100%15+1)
				p=15;
			rabbit.printCurrentPosition();
			rabbit.setxPosition(rabbit.getxPosition()+p);
			Thread.sleep(500);
		}
	}

}
